# Slippers Blog Template

The [Slippers Blog Template](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/296) warrants some specific implementation notes. 

The layout and its associated partials live in `sites/uncategorized/source/includes/cms/blog_post`

The layout uses a specific JS file, `source/javascripts/slippers-blog.js`, which [loads the Slippers CSS using webpack custom loader configuration](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/77190/diffs#5b5ceecb366e6e69e99e2bae290c68bae177fc17_0_2). There is no need to enable `extra_css` or `extra_js`. 

Since [kramdown](https://about.gitlab.com/blog/2016/07/19/markdown-kramdown-tips-and-tricks/#why-kramdown) generates markup itself, we have to [duplicate the existing blog styles in the Slippers blog component wrapper class](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1085). Those are [maintained in the `SlpBlogLayout` component's style block](https://gitlab.com/gitlab-com/marketing/inbound-marketing/slippers-ui/-/blob/master/src/components/blog-layout.vue#L34). 

We pulled in the [Storybook blog template](https://gitlab-com.gitlab.io/marketing/inbound-marketing/slippers-ui/?path=/story/templates-blog--pressure-test-1) to `sites/uncategorized/source/includes/cms/blog_post`. However, we found there were a variety of [conflicting styles](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1134#note_527410485). We attempted a few strategies to mitigate these conflicts: 

1. **Do nothing:** As we started on the integration, we wanted to see if the styles would Just Work. They didn't, that's reasonable.
1. **disable_default_styles:** we started with `disable_default_styles` set to `true` in the layout, but quickly realized that wouldn't work since we needed the default styles for the header and footer.
1. **increase specificity for Slippers:** we considered the [Tailwind configuration options for important and selector strategies](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1134#note_526726785). This resolved some of the problems, because it fixed places where our existing styles were beating Tailwind utilities, but it didn't do enough to reset or normalize styles we hadn't specified in Slippers. The about.gitlab.com styles were bleeding through and still causing problems.
1. **pull in preflight with slippers-ui:** we tried resolving the problem with [preflight](https://gitlab.com/gitlab-com/marketing/inbound-marketing/slippers-ui/-/merge_requests/37). Preflight didn't get the selector strategy/importants, and it also broke existing styles for about.gitlab.com.
1. **resolve conflicts manually:** we did this for some early iterations of the button components by using initial in the [slippers stylesheet](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/source/stylesheets/slippers.css.scss#L9). However, the breadth of issues to resolve was unmanageable for the entire layout.
1. **rebuild everything in Slippers first:** Ideally we would use Slippers and only Slippers when we build with it. Right now the header, footer, and a few partials are in the way of doing that successfully. It would add lead time to every single place we want to use Slippers.

This is a stop-gap approach that enables us to sprinkle Slippers throughout the existing `www-gitlab-com` repository.

We can use a [shadow DOM](https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_shadow_DOM) to encapsulate CSS entirely. Global styles don't leak in, local styles don't leak out. We can use [templates and slots](https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_templates_and_slots) to write semantic HTML in erb or HAML templates as desired, and we can tie it all together with [custom elements](https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements).

The browser support is there, the performance is excellent, and the value we get makes the complexity worth it. We also have fallback options available for the rare cases where web components might fail for a user.

Here's how it works: 

1. Define a [template for the blog post markup](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/77190/diffs#5667df8046398e47cb04d02fcc386420afc7ab57_35_28)
1. Since the `template` tag is valid HTML, ERB will still process everything inside it appropriately, so we can use `partial` tags and `yield` as expected - it just become part of the template.
1. Use an [inline script tag to block rendering until the script either loads or fails](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/77190/diffs#5667df8046398e47cb04d02fcc386420afc7ab57_35_53)
1. The [slippers-blog script](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/77190/diffs#diff-content-5b5ceecb366e6e69e99e2bae290c68bae177fc17) loads the [css as a variable using webpack loader syntax](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/77190/diffs#5b5ceecb366e6e69e99e2bae290c68bae177fc17_0_2), so we can inline it in the [custom element definition](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/77190/diffs#5b5ceecb366e6e69e99e2bae290c68bae177fc17_0_6)
1. We register `slp-blog` as a custom element.
1. When the DOM parses the markup, it will either render the blog post template correctly, or, in very rare circumstances, it may fail.
    1. If it fails, we fall back to the [`yield` output in the light DOM](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/77190/diffs#5667df8046398e47cb04d02fcc386420afc7ab57_35_55) to make sure critical content is never lost. 
    1. If it does not fail, the light DOM content from `yield` is thrown away on custom component render (this is why we use an inline script tag beforehand - to avoid a [flash of unstyled content](https://en.wikipedia.org/wiki/Flash_of_unstyled_content#:~:text=A%20flash%20of%20unstyled%20content,before%20all%20information%20is%20retrieved.))
1. We can use [slots to render non-Slippers items](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/77190/diffs#5667df8046398e47cb04d02fcc386420afc7ab57_40_59). Slotted elements get CSS from the light DOM, so `includes/blog/try`, `includes/blog/comments`, etc. etc. work as expected.  

This approach adds complexity and requires (a small amount of) client side rendering. It's not ideal, and we should remove it as soon as we can. 

The criteria for moving back to entirely statically generated content is: once we can set `disable_default_styles` to `true` on a Slippers page, and everything works correctly - we can avoid this approach. This probably means finished work on rebuilding the header and footer for the main site in Slippers. 
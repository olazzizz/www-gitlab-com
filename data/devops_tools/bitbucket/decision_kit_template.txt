strips:
  - template: pdf-download
    content:
      text: |
        Capability comparison conducted by Acclerated Strategies Group.  
        Each vendor's highest featured version of the product was compared.  Rated on a 0 to 5 point scale.
      link: /resources/gitlab-vs-atlassian/
  - template: bitbucket/recommendation
  - template: gitlab-advantage
  - template: carousel
    content:
      title: "GitLab in Action"
      subtitle: "Short live demos that get right to the point."
      items:
        - title: "Foster Collaboration"
          link: "https://www.youtube.com/embed/OFNUjvgm2_4"
          image: "/images/devops-tools/demo-thumbnails/foster-collaboration.png"
          #image aspect ratio should be 540x390 pixels
          type: "video"
        - title: "Benefits of a Single App"
          link: "https://www.youtube.com/embed/MNxkyLrA5Aw"
          image: "/images/devops-tools/demo-thumbnails/benefits-single-app.png"
          type: "video"
        - title: "How to Import Bitbucket Project to GitLab"
          link: "https://www.youtube.com/embed/CPcKA2OYtoQ"
          image: "/images/devops-tools/demo-thumbnails/import-bitbucket-to-gitlab.png"
          type: "video"
        - title: "Version Control - Maintain, Track and Manage Access"
          link: "https://www.youtube.com/embed/nRxCz4vMv5Q"
          image: "/images/devops-tools/demo-thumbnails/vcc-manage-access.png"
          type: "video"
        - title: "Managing Security Vulnearbilities"
          link: "https://www.youtube.com/embed/t-3TSlChHy4"
          image: "/images/devops-tools/demo-thumbnails/manage-security-vulnerabilities.png"
          type: "video"
        - title: "GitLab CI for Visibility and Collaboration"
          link: "https://www.youtube.com/embed/z8r3rFQT8xg"
          image: "/images/devops-tools/demo-thumbnails/ci-visibility-collaboration.png"
          type: "video"
  - template: webinar
  - template: carousel
    content:
      title: "5 star peer reviews on G2"
      subtitle: "Reviews by users across a broad spectrum of companies."
      items:
        - title: ""
          link: "https://www.g2.com/products/gitlab/reviews/gitlab-review-4194470"
          image: "/images/devops-tools/review-thumbnails/marton_n.png"
          type: "new_tab"
        - title: ""
          link: "https://www.g2.com/products/gitlab/reviews/gitlab-review-4183198"
          image: "/images/devops-tools/review-thumbnails/prakash_c.png"
          type: "new_tab"
        - title: ""
          link: "https://www.g2.com/products/gitlab/reviews/gitlab-review-4214537"
          image: "/images/devops-tools/review-thumbnails/haim_a.png"
          type: "new_tab"
        - title: ""
          link: "https://www.g2.com/products/gitlab/reviews/gitlab-review-4204289"
          image: "/images/devops-tools/review-thumbnails/rohit_c.png"
          type: "new_tab"
  - template: cta
  - template: carousel
    content:
      title: "Case Studies"
      subtitle: "."
      items:
        - title: '"GitLab is vastly outpacing SCM competitors such as Bitbucket."'
          link: "https://about.gitlab.com/customers/EAB/"
          image: "/images/devops-tools/casestudy-thumbnails/eab.png"
          type: "pdf"
        - title: '"Our team develops software, and we are also flying spacecraft."'
          link: "https://about.gitlab.com/customers/european-space-agency/"
          image: "/images/devops-tools/casestudy-thumbnails/esa.png"
          type: "pdf"
        - title: "If you want to speed up the delivery cycle, you need to simplify your ecosystem. And we've been doing that with GitLab."
          link: "https://about.gitlab.com/customers/axway-devops/"
          image: "/images/devops-tools/casestudy-thumbnails/axway.png"
          type: "pdf"

---
layout: handbook-page-toc
title: Ecosystem Integrations Team
description: The Ecosystem Integrations team is responsible for the Integrations category in the Ecosystem stage.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

<!-- Common Links -->
[engineering workflow]: /handbook/engineering/workflow/
[GitLab]: https://gitlab.com/gitlab-org/gitlab
[#s_ecosystem]: https://gitlab.slack.com/archives/CK4Q4709G

## About

The Integrations team is a part of the [Ecosystem Stage](https://about.gitlab.com/handbook/product/categories/#ecosystem-stage).

This page covers processes and information specific to the Integrations team.

Slack Channel (internal): [#g_ecosystem_integrations](https://gitlab.slack.com/archives/C01E55XNCEA)

## Backend Team Members

<%= direct_team(manager_role: 'Acting Backend Engineering Manager, Ecosystem:Integrations', role_regexp: /Backend/) %>

## Frontend Team Members

<%= direct_team(manager_role: 'Acting Backend Engineering Manager, Ecosystem:Integrations', role_regexp: /Frontend/) %>

## OKRs

Each quarter we have a series of Objectives and Key Results (OKRs) for our
team. To find the current OKRs for this [quarter](https://about.gitlab.com/handbook/finance/#fiscal-year), use [this issue
list](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=OKR&label_name[]=group%3A%3Aintegrations).

### Active Quarter OKRs

<iframe src="https://app.ally.io/public/obWGiKl8bTwlrNl" class="dashboard-embed" height="1100" width="100%" style="border:none;"> </iframe>

## Metrics

<%= partial("handbook/engineering/development/dev/ecosystem/integrations/metrics.erb") %>

## Work

In general, we use the standard GitLab [engineering workflow]. To get in touch with the Ecosystem:Integrations team, it's best to create an issue in the relevant project (typically [GitLab]) and add the `~"Category:Integrations"` label, along with any other appropriate labels. Then, feel free to ping the relevant Product Manager and/or Engineering Manager.

For more urgent items, feel free to use [#s_ecosystem] on Slack.

[Take a look at the features we support per category here.](/handbook/product/product-categories/features/#ecosystemintegrations-group)

### Collaborating with Counterparts

You are encouraged to work as closely as needed with stable counterparts including our PM. We should be sure to include documentation, UX, quality engineering, and application security in the planning process.

Quality engineering is included in our workflow via the [Quad Planning Process](/handbook/engineering/quality/quad-planning/).

### What to work on

<%= partial("handbook/engineering/development/dev/create/what_to_work_on.erb", locals: { group: "Integrations", slack_channel: "g_ecosystem_integrations" }) %>

[issue board]: https://gitlab.com/groups/gitlab-org/-/boards/412126?label_name[]=group%3A%3Aintegrations
[assignment board]: https://gitlab.com/groups/gitlab-org/-/boards/2168283

### Issue Boards

The work for the Integrations team can be tracked on the following issue boards:

### Workflow Boards

Workflow Boards track the workflow progress of issues.

- [BE Workflow Board](https://gitlab.com/groups/gitlab-org/-/boards/1290820?scope=all&utf8=%E2%9C%93&label_name[]=group%3A%3Aintegrations&label_name[]=backend)
- [FE Workflow Board](https://gitlab.com/groups/gitlab-org/-/boards/1290820?scope=all&utf8=%E2%9C%93&label_name[]=group%3A%3Aintegrations&label_name[]=frontend)

### Team Member Boards

Team Member Boards track `group::integrations` labeled issues by the assigned team member.

- [BE Workflow Board](https://gitlab.com/groups/gitlab-org/-/boards/2168283?scope=all&label_name[]=backend&label_name[]=group%3A%3Aintegrations)
- [FE Workflow Board](https://gitlab.com/groups/gitlab-org/-/boards/2168283?scope=all&label_name[]=frontend&label_name[]=group%3A%3Aintegrations)

### Deliverable Boards

Deliverable Boards tracks `~Deliverable` and `~Stretch` issues.

- [BE Workflow Board](https://gitlab.com/groups/gitlab-org/-/boards/412126?label_name[]=backend&label_name[]=group%3A%3Aintegrations)
- [FE Workflow Board](https://gitlab.com/groups/gitlab-org/-/boards/412126?label_name[]=frontend&label_name[]=group%3A%3Aintegrations)

### Monitoring

This is a collection of links for monitoring Ecosystem features.

#### Grafana dashboards

- [Ecosystem group dashboard](https://dashboards.gitlab.net/d/stage-groups-ecosystem/stage-groups-group-dashboard-create-ecosystem?orgId=1): Also contains links to various Kibana logs, filtered to our feature categories.
- [Worker queues](https://dashboards.gitlab.net/d/sidekiq-queue-detail/sidekiq-queue-detail?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main&var-queue=jira_connect:jira_connect_sync_branch): Switch queues with the `queue` dropdown.

#### Sentry errors

- [Matching "ServicesController"](https://sentry.gitlab.net/gitlab/gitlabcom/?query=is%3Aunresolved+ServicesController)
- [Matching "Integrations"](https://sentry.gitlab.net/gitlab/gitlabcom/?query=is%3Aunresolved+Integrations)
- [Matching "Jira"](https://sentry.gitlab.net/gitlab/gitlabcom/?query=is%3Aunresolved+Jira)

#### Kibana logs

##### JiraConnect workers

- [`JiraConnect::SyncMergeRequestWorker`](https://log.gprd.gitlab.net/goto/309f97d4a5c3e918e2c07754fefc94ee) errors.
- [`JiraConnect::SyncBranchWorker`](https://log.gprd.gitlab.net/goto/96364e957898896c4dc7e9ee5534b6de) errors.
- [`JiraConnect::SyncProjectWorker`](https://log.gprd.gitlab.net/goto/5f0e03847ddc1b074d6346199c8bc4d2) errors.
- [All JiraConnect sync worker](https://log.gprd.gitlab.net/goto/39348f2d169e6929c41dba2d6fb063ee) timeout errors.

### Workflow labels

<%= partial("handbook/engineering/development/dev/create/workflow_labels.erb", locals: { group_label: 'group::ecosystem' }) %>

### Capacity Planning

<%= partial("handbook/engineering/development/dev/create/capacity_planning.erb") %>

### Backlog Refinement

<%= partial("handbook/engineering/development/dev/ecosystem/integrations/backlog_refinement.erb") %>

### Retrospectives

<%= partial("handbook/engineering/development/dev/ecosystem/integrations/retrospectives.erb") %>

## Employee Development

Here are some resources team members can use for employee development:

- [Create Stage Professional Development](https://about.gitlab.com/handbook/engineering/development/dev/create/#professional-development)
- [Create Stage Training opportunities](https://about.gitlab.com/handbook/engineering/development/dev/create/engineers/training/)
- [GitLab Learning and Development](https://about.gitlab.com/handbook/people-group/learning-and-development/)
- [GitLab Learn](https://gitlab.edcast.com/)
- [Ecosystem Career Chats](/handbook/engineering/development/dev/ecosystem/integrations/career-chats)


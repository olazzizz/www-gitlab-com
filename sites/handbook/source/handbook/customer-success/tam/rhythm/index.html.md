---
layout: handbook-page-toc
title: "Rhythm of Business"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

### Rhythm of Business 

A successful TAM is pulled into various tasks across multiple territories and accounts. It is critical for a TAM to manage time effectively and concentrate on tasks that have the most positive impact for GitLab and the customer. 

The TAM Rhythm of Business has been designed to focus TAM efforts on the most fruitful daily, weekly, monthly, quarterly, and yearly tasks. 

This chart, not only provides the task information, but also the purpose behind each task and the associated handbook pages with additional information. 

Click here for a [downloadable version](https://lucid.app/documents/view/fd53487f-143b-420f-ae66-9e73f3505ef2) of the chart below. 

<iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://lucid.app/documents/embeddedchart/fd53487f-143b-420f-ae66-9e73f3505ef2" id="LvjclsjZrJBQ"></iframe>
